// Style inspiration: https://github.com/jonbrouse/terraform-style-guide

// create a route53 domain for k8s cluster
data "aws_route53_zone" "parent_domain" {
  name         = "ixia-netservice.net"
  private_zone = false
}

resource "aws_route53_zone" "cluster" {
  name = "${var.env}-sjwl-test.ixia-netservice.net"

  lifecycle {
    ignore_changes = ["tags"]
  }

  tags {
    ci-branch      = "${var.ci_branch}"
    ci-user-email  = "${var.ci_user_email}"
    ci-project-url = "${var.ci_project_url}"
    ci-pipeline-id = "${var.ci_pipeline_id}"
  }
}

resource "aws_route53_record" "cluster" {
  zone_id = "${data.aws_route53_zone.parent_domain.zone_id}"
  name    = "${var.env}-sjwl-test.ixia-netservice.net"
  type    = "NS"
  ttl     = "30"

  records = [
    "${aws_route53_zone.cluster.name_servers.0}",
    "${aws_route53_zone.cluster.name_servers.1}",
    "${aws_route53_zone.cluster.name_servers.2}",
    "${aws_route53_zone.cluster.name_servers.3}",
  ]
}

resource "aws_eip" "test_ip" {
}

resource "aws_route53_record" "test" {
  zone_id = "${aws_route53_zone.cluster.zone_id}"
  name    = "${var.hostname}.${var.env}-sjwl-test.ixia-netservice.net"
  type    = "A"
  ttl     = "60"

  records = ["${aws_eip.test_ip.public_ip}"]
}

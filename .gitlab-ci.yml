stages:
- init
- deploy
- destroy

variables:
 HOSTNAME: "resolveme1"

.terraform-init-script: &terraform-init-script |
  cd ${CI_PROJECT_DIR}/environments/${PROVIDER}/${REGION}
  terraform init -backend-config="bucket=slee-tfstate" -backend-config="key=${CI_PROJECT_PATH}/${CI_COMMIT_REF_NAME}/terraform-${ENV}.tfstate" -backend-config="region=${REGION}"
  export AWS_DEFAULT_REGION="${REGION}"
  export TF_VAR_ci_branch=${CI_COMMIT_REF_NAME}
  export TF_VAR_ci_user_email=${GITLAB_USER_EMAIL}
  export TF_VAR_ci_project_url=${CI_PROJECT_URL}
  export TF_VAR_ci_pipeline_id=${CI_PIPELINE_ID}
  export TF_VAR_env=${ENV}
  export TF_VAR_hostname=${HOSTNAME}

.terraform: &terraform
  image: "sjwl/tooling:latest"

.terraform-destroy: &terraform-destroy
  stage: destroy
  script:
    - *terraform-init-script
    - terraform destroy -force

.terraform-deploy: &terraform-deploy
  image: "sjwl/tooling:latest" 
  stage: deploy
  when: manual
  script:
    - *terraform-init-script  
    - terraform apply -target=aws_route53_record.test -input=false -auto-approve    
    - dig +nocmd +multiline +noall +answer any $ENV-sjwl-test.ixia-netservice.net
    - test_dns $(terraform output fqdn) 

fail-destroy:
  <<: *terraform
  <<: *terraform-destroy
  environment:
    name: fail
    action: stop
  when: manual
  variables:
    PROVIDER: "aws"
    REGION: "us-west-2"
    ENV: "fail"

fail-init:
  image: "sjwl/tooling:latest" 
  tags:
    - shared
    - docker
    - linux
  stage: init
  when: manual
  variables:
    PROVIDER: "aws"
    REGION: "us-west-2"
    ENV: "fail"  
  environment:
    name: fail
    on_stop: fail-destroy
  script:
    - *terraform-init-script  
    - terraform apply -target=aws_route53_zone.cluster -target=aws_route53_record.cluster -target=aws_eip.test_ip -input=false -auto-approve    

fail:
  <<: *terraform-deploy
  tags:
    - shared
    - docker
    - linux
  variables:
    PROVIDER: "aws"
    REGION: "us-west-2"
    ENV: "fail"  
  environment:
    name: fail
    on_stop: fail-destroy   

fail-verify-loop:
  image: "sjwl/tooling:latest" 
  tags:
    - shared
    - docker
    - linux
  stage: init
  when: manual
  variables:
    PROVIDER: "aws"
    REGION: "us-west-2"
    ENV: "fail"  
  script: 
    - test_dns $HOSTNAME.$ENV-sjwl-test.ixia-netservice.net  

pass-destroy:
  <<: *terraform
  <<: *terraform-destroy
  environment:
    name: pass
    action: stop
  when: manual
  variables:
    PROVIDER: "aws"
    REGION: "us-west-2"
    ENV: "pass"

pass-init:
  image: "sjwl/tooling:latest" 
  stage: init
  variables:
    PROVIDER: "aws"
    REGION: "us-west-2"
    ENV: "pass"  
  environment:
    name: pass
    on_stop: pass-destroy
  when: manual
  script:
    - *terraform-init-script  
    - terraform apply -target=aws_route53_zone.cluster -target=aws_route53_record.cluster -target=aws_eip.test_ip -input=false -auto-approve   

pass:
  <<: *terraform-deploy
  variables:
    PROVIDER: "aws"
    REGION: "us-west-2"
    ENV: "pass"  
  environment:
    name: pass
    on_stop: pass-destroy
  when: manual

pass-verify-loop:
  image: "sjwl/tooling:latest" 
  tags:
    - shared
    - docker
    - linux
    - gce
  stage: init
  when: manual
  variables:
    PROVIDER: "aws"
    REGION: "us-west-2"
    ENV: "pass"  
  script:
    - test_dns $HOSTNAME.$ENV-sjwl-test.ixia-netservice.net  

# ---------------------------------------------------------------------------------

.ci_functions: &ci_functions |

  # Parameter 1: the fqdn (required)
  # Parameter 2: first hop dns server (default to OS defined DNS settings)
  function test_dns() {
    MAX_WAIT=90000
    INCREMENT=30
    FQDN="$1"
    DNS_SERVER="$2"

    if [[ "$FQDN" == "" ]]; then
      echo "test_dns: missing fqdn"
      exit 1
    fi

    if [[ "$DNS_SERVER" != "" ]]; then
      DNS_SERVER="@$DNS_SERVER"
    fi

    TIME_LEFT=${MAX_WAIT}
    while [ $TIME_LEFT -gt 0 ]
    do
      [ "$(dig +short $FQDN $DNS_SERVER)" ] && break || sleep $INCREMENT
      TIME_LEFT=$((TIME_LEFT - INCREMENT))
      printf "."
    done
    if [ $TIME_LEFT -le 0 ]
    then
      printf "\n[$FQDN] Timeout: $1 didn't resolve after $MAX_WAIT seconds\n"
      exit 1
    else
      printf "\n[$FQDN] Success: $1 resolved after $((MAX_WAIT - TIME_LEFT)) seconds\n"
    fi
  }

before_script:
  - *ci_functions
  - apk add --update bind-tools


